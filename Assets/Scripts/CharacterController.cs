﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public int Lives = 3;
    public int Kills = 0;

    [HideInInspector] public MovementController Movement;
    [HideInInspector] public GunController Gun;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Bullet")
        {
            if (Lives > 0)
            {
                Lives--;
                Interface.GameUserInterface.UpdateLivesInfo(Lives);
            }
            else
            {
                Interface.FinishPanel.Show();
                Destroy(this.gameObject);
            }
        }
    }

    void Start()
    {
        Interface.GameUserInterface.UpdateLivesInfo(Lives);
    }

    void Awake()
    {
        if (Movement == null)
            Movement = GetComponent<MovementController>();

        if (Gun == null)
            Gun = GetComponentInChildren<GunController>();
    }
}
