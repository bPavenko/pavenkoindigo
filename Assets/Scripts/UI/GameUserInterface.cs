﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUserInterface : MonoBehaviour
{
    [Header("Game info:")]
    public Text LivesText;
    public Text PlayersText;

    [Space] [Header("Panels:")]
    public GameObject InstructionsPanel;

    public void PauseButton_Click()
    {
        Interface.MenuPanel.Show();
    }

    public void RestartInterface()
    {
        LivesText.text = "Lives: 3";
        Interface.MenuPanel.Hide();
    }

    public void UpdateLivesInfo(int lives)
    {
        LivesText.text = "Lives: " + lives;
    }

    public void UpdatePlayersInfo(int players)
    {
        PlayersText.text = "Players: " + players;
    }

    void Update()
    {
        if (InstructionsPanel.activeSelf && Input.anyKeyDown)
        {
            InstructionsPanel.SetActive(false);
            GameManager.Instance.RestartGame();
        }
    }

    void Awake()
    {
        Interface.GameUserInterface = this;
    }
}
