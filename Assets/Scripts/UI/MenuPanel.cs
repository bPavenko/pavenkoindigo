﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPanel : MonoBehaviour
{
    public GameObject Panel;
    
    public void Show()
    {
        Panel.SetActive(true);
    }

    public void Hide()
    {
        Panel.SetActive(false);
    }

    public void ContinueButton_Click()
    {
        Hide();
    }

    public void RestartButton_Click()
    {
        GameManager.Instance.RestartGame();
    }

    public void ExitButton_Click()
    {
        Application.Quit();
    }

    void OnValidate()
    {
        if (Panel == null)
            Panel = GetComponentInChildren<GameObject>();
    }

    void Awake()
    {
        Interface.MenuPanel = this;
    }
}
