﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinishPanel : MonoBehaviour
{
    public GameObject Panel;

    [Header("Texts objects:")]
    public GameObject WinText;
    public GameObject LoseText;
    public Text KillsText;

    public void RestartButton_Click()
    {
        GameManager.Instance.RestartGame();
        Panel.SetActive(false);
    }

    public void ExitButton_Click()
    {
        Application.Quit();
    }

    public void Show()
    {
        GameManager.Instance.Play = false;
        Panel.SetActive(true);
        if(GameManager.Instance.PlayerController.Kills == GameManager.Instance.EnemyCount)
            WinText.SetActive(true);
        else
            LoseText.SetActive(true);

        KillsText.text = "Kills: " + GameManager.Instance.PlayerController.Kills;
    }

    void Awake()
    {
        Interface.FinishPanel = this;
    }
}
