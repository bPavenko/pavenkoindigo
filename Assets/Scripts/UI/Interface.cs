﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Interface
{
    public static MenuPanel MenuPanel;
    public static GameUserInterface GameUserInterface;
    public static FinishPanel FinishPanel;
}
