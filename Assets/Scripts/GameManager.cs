﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [Header("Prefabs:")]
    public GameObject EnemyPrefab;
    public GameObject PlayerPrefab;

    [Space] [Header("Game settings:")]
    public int EnemyCount = 10;
    [HideInInspector]
    public bool Play = false;
    [HideInInspector]
    public CharacterController PlayerController;

    private List<GameObject> _enemyList = new List<GameObject>();

    public void RemoveDeath(GameObject enemyObj)
    {
        PlayerController.Kills++;
        _enemyList.Remove(enemyObj);
        Destroy(enemyObj);
        Interface.GameUserInterface.UpdatePlayersInfo(_enemyList.Count);
    }

    public void RestartGame()
    {
        //Cleaning
        Interface.GameUserInterface.RestartInterface();
        if (_enemyList.Count > 0)
        {
            foreach (GameObject enemy in _enemyList)
            {
                Destroy(enemy);
            }
        }

        //Creating
        PlayerCreate();
        GenerateEnemy();
        Play = true;
    }

    public void GenerateEnemy()
    {
        for (int i = 0; i < EnemyCount; i++)
        {
            GameObject newEnemy = Instantiate(EnemyPrefab, GeneratePosition(), Quaternion.identity);
            _enemyList.Add(newEnemy);
        }
    }

    private void PlayerCreate()
    {
        if (PlayerController != null)
        {
            Destroy(PlayerController.gameObject);
            PlayerController = null;
        }

        GameObject player = Instantiate(PlayerPrefab, GeneratePosition(), Quaternion.identity);
        PlayerController = player.GetComponent<CharacterController>();
        InputController.Instance.Player = PlayerController;
    }

    private Vector3 GeneratePosition()
    {
        Vector3 pos = new Vector3(Random.Range(-15f, 15f), 0f, Random.Range(-15f, 15f));
        return pos;
    }

    void Start()
    {
        Interface.GameUserInterface.UpdatePlayersInfo(EnemyCount);
    }

    void Awake()
    {
        Instance = this;
    }
}
