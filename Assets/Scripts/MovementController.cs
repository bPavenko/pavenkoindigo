﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float Speed = 1f;
    public bool IsEnemy = false;
    public Vector3 MovingVector;

    private Rigidbody _rigidbody;
    
    public void Move(Vector3 vector3)
    {
        if (vector3 != Vector3.zero)
            MovingVector = vector3;
        else
            MovingVector = Vector3.zero;
    }

    public void SetMoveVector()
    {
        MovingVector = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)); 
    }

    void OnCollisionEnter(Collision collision)
    {
        if(IsEnemy && (collision.transform.tag == "border" || collision.transform.tag == "Enemy" || collision.transform.tag == "Player"))
            SetMoveVector();
    }

    void Start()
    {
        SetMoveVector();
    }

	void FixedUpdate () {
        if(MovingVector != Vector3.zero) transform.rotation = Quaternion.LookRotation(MovingVector);
        _rigidbody.velocity = MovingVector * Speed;
	    _rigidbody.angularVelocity = MovingVector * Speed;
	}

    void Awake()
    {
        if (_rigidbody == null)
            _rigidbody = this.GetComponent<Rigidbody>();
    }
}
