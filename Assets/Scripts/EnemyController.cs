﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float ShootTick = 5f;
    public float SetMoveTick = 7f;
    public int Lives = 3;

    [HideInInspector]
    public MovementController Movement;

    private GunController _gun;

    private float _shootTimer;
    private float _setMoveTimer;

    void Damage()
    {
        if (Lives > 0)
            Lives--;
        else
        {
            GameManager.Instance.RemoveDeath(this.gameObject);
            if (GameManager.Instance.PlayerController.Kills == GameManager.Instance.EnemyCount)
            {
                Interface.FinishPanel.Show();
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Bullet")
            Damage();
    }

    void FixedUpdate()
    {
        if (_shootTimer > 0)
            _shootTimer -= Time.deltaTime;
        else
        {
            _gun.Shoot();
            _shootTimer = Random.Range(0f, ShootTick);
        }

        if (_setMoveTimer > 0)
            _setMoveTimer -= Time.deltaTime;
        else
        {
            Movement.SetMoveVector();
            _setMoveTimer = SetMoveTick;
        }
    }

    void Start()
    {
        _shootTimer = ShootTick;
    }

    void Awake()
    {
        if (Movement == null)
            Movement = GetComponent<MovementController>();

        if (_gun == null)
            _gun = GetComponentInChildren<GunController>();
    }
}
