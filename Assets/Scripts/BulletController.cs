﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float Speed = 10f;

    [HideInInspector]
    public Vector3 TargetVector = Vector3.zero;

    private Rigidbody _rigidbody;
    private SphereCollider _collider;
    private float _enableColliderTimer = 0.3f;

    void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }

    void FixedUpdate()
    {
        if (_enableColliderTimer > 0)
            _enableColliderTimer -= Time.deltaTime;
        else
            _collider.isTrigger = false;

        if (TargetVector != Vector3.zero) transform.rotation = Quaternion.LookRotation(TargetVector);
        _rigidbody.velocity = TargetVector * Speed;
        _rigidbody.angularVelocity = TargetVector * Speed;

        if(transform.position.x > 18 || transform.position.x < -18 || transform.position.z > 18 || transform.position.z < -18)
            Destroy(this.gameObject);
    }

    void Awake()
    {
        if (_collider == null)
            _collider = GetComponent<SphereCollider>();

        if (_rigidbody == null)
            _rigidbody = GetComponent<Rigidbody>();
    }
}
