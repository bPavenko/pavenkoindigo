﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public GameObject BulletPrefab;

    private CharacterController _characterController;
    private EnemyController _enemyController;

    public void Shoot()
    {
        if (_characterController)
        {
            GameObject bullet = Instantiate(BulletPrefab,
                new Vector3(_characterController.transform.position.x, 1.5f, _characterController.transform.position.z),
                Quaternion.identity);
            if (_characterController.Movement.MovingVector != Vector3.zero)
                bullet.GetComponent<BulletController>().TargetVector = _characterController.Movement.MovingVector;
            else
                bullet.GetComponent<BulletController>().TargetVector = Vector3.forward;
        }
        else
        {
            GameObject bullet = Instantiate(BulletPrefab,
                new Vector3(_enemyController.transform.position.x, 1.5f, _enemyController.transform.position.z),
                Quaternion.identity);
            if (_enemyController.Movement.MovingVector != Vector3.zero)
                bullet.GetComponent<BulletController>().TargetVector = _enemyController.Movement.MovingVector;
            else
                bullet.GetComponent<BulletController>().TargetVector = Vector3.forward;
        }

    }

    void Awake()
    {
        if (_characterController == null)
            _characterController = GetComponentInParent<CharacterController>();

        if (_enemyController == null)
            _enemyController = GetComponentInParent<EnemyController>();
    }
}
