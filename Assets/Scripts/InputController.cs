﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static InputController Instance;
    [HideInInspector]
    public CharacterController Player;

	void Update ()
	{
        if (GameManager.Instance.Play)
            Player.Movement.Move(new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")));

        if (Player != null && (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)))
            Player.Gun.Shoot();
    }

    void Awake()
    {
        Instance = this;
    }
}
